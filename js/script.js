function changeActiveCard(clickedCard) {
  let activeCard = document.querySelector("div.active-card");
  activeCard.classList.remove("active-card");
  clickedCard.classList.add("active-card");
}

function togglePlan(toggleBtn) {
  toggleBtn.classList.toggle("toggle-active");
  let basePrice = document.getElementById("base-price");
  let professionalPrice = document.getElementById("professional-price");
  let masterPrice = document.getElementById("master-price");
  console.log(professionalPrice.innerText);
  if (basePrice.innerText == "$19.99") basePrice.innerText = "$199.99";
  else basePrice.innerText = "$19.99";
  if (professionalPrice.innerText == "$24.99")
    professionalPrice.innerText = "$249.99";
  else professionalPrice.innerText = "$24.99";
  if (masterPrice.innerText == "$39.99") masterPrice.innerText = "$399.99";
  else masterPrice.innerText = "$39.99";
}

let card1 = document.getElementById("card-1");
let card2 = document.getElementById("card-2");
let card3 = document.getElementById("card-3");

let toggleBtn = document.getElementById("toggle");
card1.addEventListener("click", () => {
  changeActiveCard(card1);
});
card2.addEventListener("click", () => {
  changeActiveCard(card2);
});

card3.addEventListener("click", () => {
  changeActiveCard(card3);
});

toggleBtn.addEventListener("click", () => {
  togglePlan(toggleBtn);
});
